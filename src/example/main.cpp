// See: https://www.youtube.com/watch?v=6-0UaeJBumA
#include <SDL2/SDL.h>

#include <cmath>
#include <ctime>

#include <iostream>

#include "NoiseGenerator/perlin1d.h"
#include "NoiseGenerator/perlin2d.h"
#include "NoiseGenerator/perlin-like.h"

constexpr uint32_t WHITE = 255 << 24 | 255 << 16 | 255 << 8;

double max(PerlinNoise1D& noise) {
	double max = noise[0];

	for(unsigned long i = 1; i < noise.size(); ++i) {
		double val = noise[i];

		if( val > max )
			max = val;
	}

	return max;
}

double max(PerlinNoise2D& noise) {
	double max = noise[0];

	for(unsigned long i = 1; i < noise.size(); ++i) {
		double val = noise[i];

		if( val > max )
			max = val;
	}

	return max;
}

template<typename T>
T max(T *noise, const unsigned long size) {
	double max = noise[0];

	for(unsigned long i = 1; i < size; ++i) {
		T val = noise[i];

		if( val > max )
			max = val;
	}

	return max;
}

typedef enum {
	MODE_1D,
	MODE_2D,
	MODE_3D,
} Mode;

int f_red(double c) {
	return (int)round(c * 255) << 24;
}

int f_red_inv(double c) {
	return (255 - (int)round(c * 255)) << 24;
}

int f_green(double c) {
	return (int)round(c * 255) << 16;
}

int f_green_inv(double c) {
	return ( 255 - (int)round(c * 255) ) << 16;
}

int f_blue(double c) {
	return (int)round(c * 255) << 8;
}

int f_blue_inv(double c) {
	return ( 255 - (int)round(c * 255) ) << 8;
}

int f_alpha(double c) {
	return (int)round(c * 255);
}

int f_alpha_inv(double c) {
	return ( 255 - (int)round(c * 255) );
}

uint32_t toColorLinear(double c) {
	return f_red(c) | f_green(c) | f_blue(c);
}

uint32_t toColorScale(double c) {
	return f_green(c) | f_blue_inv(c);
}

void fillMap(PerlinNoise1D g, Uint32 *arr, const unsigned long width, unsigned long height) {
	for(unsigned int x = 0; x < width; ++x) {
		const int y = -(g[x] * height / 2.0) + height / 2.0;
		for(unsigned int f = y; f < height / 2; ++f) {
			arr[f * width + x] = WHITE;
		}
	}
}

void fillMap(PerlinNoise2D g, Uint32 *arr, const unsigned long length) {
	for (unsigned long i = 0; i < length; ++i) {
		arr[i] = toColorScale(g[i]);
	}
}

template<typename rtype, typename toto, int freq>
void fillMap(PerlinLike<rtype, toto, freq> g, Uint32 *arr, const unsigned long width, const unsigned long height) {
	for(unsigned long x = 0; x < width; x++){
		for (unsigned long y = 0; y < height; y++) {
			unsigned long idx = x + y * width;
			arr[idx] = toColorScale(g(x, y));
		}
	}
}

template<typename rtype, typename toto, int freq>
void fillMap(PerlinLikeSimple<rtype, toto, freq> g, Uint32 *arr, const unsigned long width, const unsigned long height) {
	for(unsigned long x = 0; x < width; x++){
		for (unsigned long y = 0; y < height; y++) {
			unsigned long idx = x + y * width;
			arr[idx] = toColorScale(g(x, y));
		}
	}
}

int main(int argc, char *argv[]) {
	SDL_Init(SDL_INIT_VIDEO);

	constexpr unsigned int width = 1440;
	constexpr unsigned int height = 980;

	SDL_Window   *window   = SDL_CreateWindow("Perlin", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, SDL_WINDOW_SHOWN);
	SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_Texture  *texture  = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_STREAMING, width, height);

	PerlinNoise1D noise1d(width, time(nullptr));
	PerlinNoise2D noise2d(width, height, time(nullptr));
	// PerlinLike<double, std::ranlux24, 8> plike(time(nullptr));
	PerlinLikeSimple<double, std::ranlux24, 8> plike(time(nullptr));

	Uint32 pixels[width * height] = { 0 };
	constexpr unsigned int fps = 30;
	constexpr unsigned int delay = 1000 / fps;
	unsigned int octaves = 1;
	double bias = 2.0;
	Mode mode = MODE_1D;
	bool running = true;
	bool regenerate = true;

	while( running ) {
		Uint32 startLoop = SDL_GetTicks();

		SDL_RenderClear(renderer);
		SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

		SDL_Event evt;
		while( SDL_PollEvent(&evt) ) {
			switch(evt.type) {
				case SDL_QUIT:
					running = false;
					break;

				case SDL_KEYUP:
					if( evt.key.keysym.sym == SDLK_ESCAPE ) {
						running = false;
					} else if( evt.key.keysym.sym == SDLK_z ) {
						noise1d.generateSeed();
						noise2d.generateSeed();
						regenerate = true;
					} else if( evt.key.keysym.sym == SDLK_a ) {
						bias += 0.2;
						regenerate = true;
					} else if( evt.key.keysym.sym == SDLK_q ) {
						bias -= 0.2;
						if( bias < 0.2 )
							bias = 0.2;
						regenerate = true;
					} else if( evt.key.keysym.sym == SDLK_SPACE ) {
						octaves += 1;
						if( octaves > 10 )
							octaves = 1;
						regenerate = true;
					} else if( evt.key.keysym.sym == SDLK_1 ) {
						if( mode != MODE_1D ) {
							regenerate = true;
							mode = MODE_1D;
						}
					} else if( evt.key.keysym.sym == SDLK_2 ) {
						if( mode != MODE_2D ) {
							regenerate = true;
							mode = MODE_2D;
						}
					} else if( evt.key.keysym.sym == SDLK_3 ) {
						if( mode != MODE_3D ) {
							regenerate = true;
							mode = MODE_3D;
						}
					}

				default:
					break;
			}
		}

		if( regenerate ) {
			memset(pixels, 0, width * height * sizeof(Uint32));

			switch(mode) {
				case MODE_1D:
					noise1d.generate(octaves, bias);
					fillMap(noise1d, pixels, width, height);
					std::cerr << "MODE_1D: Octaves: "       << octaves    << std::endl;
					std::cerr << "MODE_1D: Bias: "	  << bias       << std::endl;
					std::cerr << "MODE_1D: Maximum value: " << max(noise1d) << std::endl;
					std::cerr << "MODE_1D: Maximum pixel value: " << max(pixels, width * height) << std::endl;

					break;
				case MODE_2D:
					noise2d.generate(octaves, bias);
					fillMap(noise2d, pixels, width * height);
					std::cerr << "MODE_2D: Octaves: "       << octaves    << std::endl;
					std::cerr << "MODE_2D: Bias: "	  << bias       << std::endl;
					std::cerr << "MODE_2D: Maximum value: " << max(noise2d) << std::endl;
					std::cerr << "MODE_2D: Maximum pixel value: " << max(pixels, width * height) << std::endl;
					break;
				case MODE_3D:
					fillMap(plike, pixels, width, height);
					std::cerr << "MODE_3D: Maximum pixel value: " << max(pixels, width * height) << std::endl;
					break;
			}

			regenerate = false;
		}

		SDL_UpdateTexture(texture, NULL, pixels, width * sizeof(Uint32));

		SDL_RenderCopy(renderer, texture, NULL, NULL);

		SDL_RenderPresent(renderer);

		const Uint32 remain = SDL_GetTicks() - startLoop;
		std::cerr << "remaining: " << remain << std::endl;
		if( remain < delay )
			SDL_Delay(delay - remain);
	}

	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}

#include "NoiseGenerator/perlin2d.h"

PerlinNoise2D::PerlinNoise2D(const unsigned long width, const unsigned height, const long seed): Generator(), _seed(seed), _width(width), _height(height), _size(width * height) {
	// srand(this->_seed);
	// srandom(this->_seed);

	this->_noiseSeed = new double[this->_size];
	this->_data = new double[this->_size];

	this->generateSeed();
}

void PerlinNoise2D::generateSeed(void) {
	for(unsigned long i = 0; i < this->_size; ++i) {
		this->_noiseSeed[i] = ((double)std::rand()) / RAND_MAX;
		// this->_noiseSeed[i] = ((double)random()) / RAND_MAX;
	}
}

void PerlinNoise2D::generate(void) {
	this->generate(10, 2.0);
}

void PerlinNoise2D::generate(const unsigned int octaves, const double bias) {
	for(unsigned long x = 0; x < this->_width; x++) {
		for(unsigned long y = 0; y < this->_height; y++) {
			double noise = 0.0;
			double scale = 1.0;
			double scaleAcc = 0.;

			for(unsigned int o = 0; o < octaves; o++) {
				// Calculate the distance betwwen the two point to interpolate between:
				unsigned int pitch = this->_width >> o; // divide by the number of octave

				// Foind out which point will be our first sample:
				unsigned int sample1_x = (x / pitch) * pitch;
				unsigned int sample1_y = (y / pitch) * pitch;
				// Our second sample:
				unsigned int sample2_x = (sample1_x + pitch) % this->_width;
				unsigned int sample2_y = (sample1_y + pitch) % this->_width;

				// Calculating the "blend":
				double blend_x = (double)(x - sample1_x) / (double)pitch;
				double blend_y = (double)(y - sample1_y) / (double)pitch;

				// Interpolate between our two sample:
				double sampleT = (1.0 - blend_x) * this->_noiseSeed[sample1_y * this->_width + sample1_x] + blend_x * this->_noiseSeed[sample1_y * this->_width + sample2_x];
				double sampleB = (1.0 - blend_x) * this->_noiseSeed[sample2_y * this->_width + sample1_x] + blend_x * this->_noiseSeed[sample2_y * this->_width + sample2_x];

				scaleAcc += scale;
				noise += (blend_y * (sampleB - sampleT) + sampleT) * scale;
				scale /= bias;
			}

			this->_data[y * this->_width + x] = noise / scaleAcc;
		}
	}
}

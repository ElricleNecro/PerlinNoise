// See: https://www.youtube.com/watch?v=6-0UaeJBumA
#include <SDL2/SDL.h>

#include <cmath>
#include <ctime>

#include <iostream>

#include "NoiseGenerator/perlin1d.h"
#include "NoiseGenerator/perlin2d.h"
#include "NoiseGenerator/perlin-like.h"

int f_red(double c) {
	return (int)round(c * 255) << 24;
}

int f_red_inv(double c) {
	return (255 - (int)round(c * 255)) << 24;
}

int f_green(double c) {
	return (int)round(c * 255) << 16;
}

int f_green_inv(double c) {
	return ( 255 - (int)round(c * 255) ) << 16;
}

int f_blue(double c) {
	return (int)round(c * 255) << 8;
}

int f_blue_inv(double c) {
	return ( 255 - (int)round(c * 255) ) << 8;
}

int f_alpha(double c) {
	return (int)round(c * 255);
}

int f_alpha_inv(double c) {
	return ( 255 - (int)round(c * 255) );
}

uint32_t toColorLinear(double c) {
	return f_red(c) | f_green(c) | f_blue(c);
}

uint32_t toColorScale(double c) {
	return f_green(c) | f_blue_inv(c);
}

template<typename rtype, typename toto, int freq>
void fillMap(PerlinLike<rtype, toto, freq> g, Uint32 *arr, const unsigned long width, const unsigned long height) {
	for(unsigned long x = 0; x < width; x++){
		for (unsigned long y = 0; y < height; y++) {
			unsigned long idx = x + y * width;
			arr[idx] = toColorScale(g(x, y));
		}
	}
}

template<typename rtype, typename toto, int freq>
void fillMap(PerlinLikeSimple<rtype, toto, freq> g, Uint32 *arr, const unsigned long width, const unsigned long height) {
	for(unsigned long x = 0; x < width; x++){
		for (unsigned long y = 0; y < height; y++) {
			unsigned long idx = x + y * width;
			arr[idx] = toColorScale(g(x, y));
		}
	}
}

int main(int argc, char *argv[]) {
	constexpr unsigned int width  = 256;
	constexpr unsigned int height = 256;

	PerlinLikeSimple<double, std::ranlux24, 8> plike(time(nullptr));
	Uint32 pixels[width * height] = { 0 };

	fillMap(plike, pixels, width, height);

	return EXIT_SUCCESS;
}

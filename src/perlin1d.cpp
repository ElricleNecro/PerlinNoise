#include "NoiseGenerator/perlin1d.h"

PerlinNoise1D::PerlinNoise1D(const unsigned long size, const long seed): Generator(), _seed(seed), _size(size) {
	// srand(this->_seed);
	srandom(this->_seed);

	this->_noiseSeed = new double[size];
	this->_data = new double[size];

	this->generateSeed();
}

void PerlinNoise1D::generateSeed(void) {
	for (unsigned long i = 0; i < this->_size; ++i) {
		// this->_noiseSeed[i] = ((double)rand()) / RAND_MAX;
		this->_noiseSeed[i] = ((double)random()) / RAND_MAX;
	}
}

void PerlinNoise1D::generate(void) {
	this->generate(10, 2.0);
}

void PerlinNoise1D::generate(const unsigned int octaves, const double bias) {
	for(unsigned long x = 0; x < this->_size; x++) {
		double noise = 0.0;
		double scale = 1.0;
		double scaleAcc = 0.;

		for(unsigned int o = 0; o < octaves; o++) {
			// Calculate the distance betwwen the two point to interpolate between:
			int pitch = this->_size >> o; // divide by the number of octave

			// Foind out which point will be our first sample:
			int sample1 = (x / pitch) * pitch;
			// Our second sample:
			int sample2 = (sample1 + pitch) % this->_size;

			// Calculating the "blend":
			double blend = (double)(x - sample1) / (double)pitch;
			// Interpolate between our two sample:
			double sample = (1.0 - blend) * this->_noiseSeed[sample1] + blend * this->_noiseSeed[sample2];

			noise += sample * scale;
			scaleAcc += scale;
			scale /= bias;
		}

		this->_data[x] = noise / scaleAcc;
	}
}

-- io.input("config.h.cmake")
-- local text = io.read("*a")

-- text = string.gsub(text, "@PLUGIN_API_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@PLUGIN_API_VERSION_MINOR@", "1")

-- text = string.gsub(text, "@AI_VERSION_MAJOR@", "0")
-- text = string.gsub(text, "@AI_VERSION_MINOR@", "1")

-- io.output("include/config.hpp")
-- io.write(text)
-- io.close()

solution("ParadoxTest")
	configurations({"debug", "release"})
		buildoptions(
			{
				"-std=c++17"
			}
		)

		flags(
			{
				"ExtraWarnings"
			}
		)

	includedirs(
		{
			"include/"
		}
	)

	configuration("debug")
		buildoptions(
			{
				"-g",
				"-pg",
			}
		)
		flags(
			{
				"Symbols"
			}
		)
		linkoptions(
			{
				"-pg"
			}
		)

	configuration("release")
		buildoptions(
			{
				"-O3"
			}
		)

	project("RandomNoise")
		language("C++")
		kind("SharedLib")

		location("build/lib")
		targetdir("build/lib")

		files(
			{
				"src/*.cpp"
			}
		)

		includedirs(
			{
				"include/"
			}
		)

		links(
			{
			}
		)

	project("perlin_visualiser")
		language("C++")
		kind("ConsoleApp")

		location("build/bin")
		targetdir("build/bin")

		buildoptions({"`sdl2-config --cflags`"})
		linkoptions({"`sdl2-config --libs`", "-lRandomNoise"})

		files(
			{
				"src/example/*.cpp"
			}
		)
		libdirs(
			{
				"build/lib"
			}
		)

	project("prof")
		language("C++")
		kind("ConsoleApp")

		location("build/bin")
		targetdir("build/bin")

		buildoptions({"-pg"})
		linkoptions({"-pg", "-lRandomNoise"})

		files(
			{
				"src/profiling/*.cpp"
			}
		)
		libdirs(
			{
				"build/lib"
			}
		)

	-- project("Queue")
		-- language("C++")
		-- kind("SharedLib")

		-- location("build/lib")
		-- targetdir("build/lib")

		-- files(
			-- {
				-- "src/Queue/*.cpp"
			-- }
		-- )

#ifndef PERLIN2D_H_UCIQTPME
#define PERLIN2D_H_UCIQTPME

#include <algorithm>

#include "NoiseGenerator/generator.h"

class PerlinNoise2D : public Generator {
	public:
		PerlinNoise2D(const unsigned long width, const unsigned height, const long seed = 12310924221L);
		virtual ~PerlinNoise2D(void) override {};

		void generateSeed(void);
		void generate(void) override;
		void generate(const unsigned int octaves=10, const double bias=2.0);

		inline unsigned long size(void) const { return this->_size; };
		double operator[](const unsigned int idx) const override { return this->_data[idx]; };

	private:
		/* data */
		long _seed;
		unsigned long _width, _height, _size;

		double *_noiseSeed = nullptr;
		double *_data = nullptr;
};

#endif /* end of include guard: PERLIN2D_H_UCIQTPME */

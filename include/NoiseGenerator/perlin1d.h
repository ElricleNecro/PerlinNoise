#ifndef PERLIN_H_DMBK63ID
#define PERLIN_H_DMBK63ID

#include <cstdlib>

#include "NoiseGenerator/generator.h"

class PerlinNoise1D : public Generator {
	public:
		PerlinNoise1D(const unsigned long size, const long seed = 12310924221L);
		virtual ~PerlinNoise1D(void) override {};

		void generateSeed(void);
		void generate(void) override;
		void generate(const unsigned int octaves=10, const double bias=2.0);

		inline unsigned long size(void) const { return this->_size; };
		double operator[](const unsigned int idx) const override { return this->_data[idx]; };

	private:
		/* data */
		long _seed;
		unsigned long _size;

		double *_noiseSeed = nullptr;
		double *_data = nullptr;
};

#endif /* end of include guard: PERLIN_H_DMBK63ID */

#ifndef PERLIN_H
#define PERLIN_H

#include <cmath>
#include <random>
#include <iostream>

#include "NoiseGenerator/generator.h"

template<typename T, typename _Gen=std::default_random_engine, int freq=8>
class PerlinLike {
	public:
		PerlinLike(unsigned long seed, const T amplitude = T(70.)) : _seed(seed), _generator(seed), _dis(T(0.), T(1.)), _amplitude(amplitude) {
			std::cerr << this->getNoise(5, 14) << std::endl;
			std::cerr << this->getNoise(5, 14) << std::endl;
			std::cerr << this->getNoise(5, 14) << std::endl;
			std::cerr << this->getNoise(6, 14) << std::endl;
		};

		inline T operator()(const long x, const long y) {
			return this->getInterpolatedNoise(x / freq, y / freq) * this->_amplitude;
		};

	private:
		inline T getNoise(const long x, const long y) {
			this->_generator.seed(x * 49632 + y * 325176 + this->_seed);
			return this->_dis(this->_generator) * T(2) - T(1);
		};

		inline T getSmoothNoise(const long x, const long y) {
			T corners = (
				this->getNoise(x-1, y-1) +
				this->getNoise(x+1, y-1) +
				this->getNoise(x-1, y+1) +
				this->getNoise(x+1, y+1)
			) / T(16);
			T sides = (
				this->getNoise(x-1, y) +
				this->getNoise(x+1, y) +
				this->getNoise(x, y-1) +
				this->getNoise(x, y+1)
			) / T(8);

			return corners + sides + (this->getNoise(x, y) / T(4));
		};

		inline T interpolate(T a, T b, T blend) {
			T theta = blend * std::acos(-1.0);
			T f = (T(1) - std::cos(theta)) * T(0.5);
			return a * (T(1) - f) + b * f;
		};

		T getInterpolatedNoise(const T x, const T y) {
			long ix = (long) x;
			long iy = (long) y;

			T fracX = x - ix;
			T fracY = y - iy;

			T v1 = this->getSmoothNoise(ix, iy);
			T v2 = this->getSmoothNoise(ix + 1, iy);
			T v3 = this->getSmoothNoise(ix, iy + 1);
			T v4 = this->getSmoothNoise(ix + 1, iy + 1);

			T i1 = this->interpolate(v1, v2, fracX);
			T i2 = this->interpolate(v3, v4, fracX);

			return this->interpolate(i1, i2, fracY);
		};

	private:
		unsigned long _seed;

		_Gen _generator;
		std::uniform_real_distribution<T> _dis;

		T _amplitude;
};

template<typename T, typename _Gen=std::default_random_engine, int freq=8>
class PerlinLikeSimple {
	public:
		PerlinLikeSimple(unsigned long seed, const T amplitude = T(70.)) : _seed(seed), _amplitude(amplitude) {
			std::cerr << this->getNoise(5, 14) << std::endl;
			std::cerr << this->getNoise(5, 14) << std::endl;
			std::cerr << this->getNoise(5, 14) << std::endl;
			std::cerr << this->getNoise(6, 14) << std::endl;
		};

		inline T operator()(const long x, const long y) {
			return this->getInterpolatedNoise(x / freq, y / freq) * this->_amplitude;
		};

	private:
		inline T getNoise(const long x, const long y) {
			std::srand(x * 49632 + y * 325176 + this->_seed);
			return std::rand() / (RAND_MAX + 1.) * 2. - 1.;
			// this->_generator.seed(x * 49632 + y * 325176 + this->_seed);
			// return this->_dis(this->_generator) * T(2) - T(1);
		};

		inline T getSmoothNoise(const long x, const long y) {
			T corners = (
				this->getNoise(x-1, y-1) +
				this->getNoise(x+1, y-1) +
				this->getNoise(x-1, y+1) +
				this->getNoise(x+1, y+1)
			) / T(16);
			T sides = (
				this->getNoise(x-1, y) +
				this->getNoise(x+1, y) +
				this->getNoise(x, y-1) +
				this->getNoise(x, y+1)
			) / T(8);

			return corners + sides + (this->getNoise(x, y) / T(4));
		};

		inline T interpolate(T a, T b, T blend) {
			T theta = blend * std::acos(-1.0);
			T f = (T(1) - std::cos(theta)) * T(0.5);
			return a * (T(1) - f) + b * f;
		};

		T getInterpolatedNoise(const T x, const T y) {
			long ix = (long) x;
			long iy = (long) y;

			T fracX = x - ix;
			T fracY = y - iy;

			T v1 = this->getSmoothNoise(ix, iy);
			T v2 = this->getSmoothNoise(ix + 1, iy);
			T v3 = this->getSmoothNoise(ix, iy + 1);
			T v4 = this->getSmoothNoise(ix + 1, iy + 1);

			T i1 = this->interpolate(v1, v2, fracX);
			T i2 = this->interpolate(v3, v4, fracX);

			return this->interpolate(i1, i2, fracY);
		};

	private:
		unsigned long _seed;

		T _amplitude;
};

#endif /* PERLIN_H */

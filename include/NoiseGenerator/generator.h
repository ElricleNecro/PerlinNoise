#ifndef GENERATOR_H_VIQFJMPN
#define GENERATOR_H_VIQFJMPN

class Generator {
	public:
		Generator(void) {};
		virtual ~Generator(void) {};

		virtual void generate(void) = 0;

		virtual double operator[](const unsigned int idx) const = 0;
};

#endif /* end of include guard: GENERATOR_H_VIQFJMPN */
